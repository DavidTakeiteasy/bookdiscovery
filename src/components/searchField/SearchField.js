import React from "react";
import styles from "./searchField.module.scss";

const SearchField = ({ searchValue, setSearchValue, fetchData, itemValue, setPageValue}) => {
    const handleChange = (event) => {
        setSearchValue(event.target.value);
    };

    const handleSearchValue = () => {
        setSearchValue(searchValue);
        searchValue && fetchData(itemValue, 1);
        setPageValue(1);
        goToTop();
    };

    const goToTop = () => {
        window.scrollTo({
            top: 0,
            behavior: "smooth",
        });
    };

    return (
        <div className={styles.searchField}>
            <input
                type="text"
                id="message"
                name="message"
                onChange={handleChange}
                value={searchValue}
            />

            <button onClick={handleSearchValue}>Search Books</button>
        </div>
    );
};

export default SearchField;
