import React from "react";
import styles from "./viewOption.module.scss";
import iconGrid from '../../images/iconGrid.png';
import iconList from '../../images/iconList.png';

const ViewOption = ({ viewOption, setViewOption, configuration }) => {
    const handleChange = (value) => {
        setViewOption(value);
    };

    return (
        <nav className={styles.viewOption}>
            <div className={styles.buttons}>
                <button
                    onClick={() => handleChange(configuration[0])}
                    className={viewOption === configuration[0] ? styles.active : undefined}
                >
                    <img src={iconGrid} alt="" />
                </button>
                <button
                    onClick={() => handleChange(configuration[1])}
                    className={viewOption === configuration[1] ? styles.active : undefined}
                >
                    <img src={iconList} alt="" />
                </button>
            </div>
        </nav>
    );
};

export default ViewOption;
