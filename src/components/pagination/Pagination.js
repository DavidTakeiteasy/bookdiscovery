import React from "react";
import styles from "./pagination.module.scss";

const Pagination = ({ itemValue, pageValue, setPageValue, total, fetchData }) => {
    const handleClickBack = () => {
        fetchData(itemValue, pageValue - 1);
        setPageValue(pageValue - 1);
        goToTop();
    };
    const handleClickNext = () => {
        fetchData(itemValue, pageValue + 1);
        setPageValue(pageValue + 1);
        goToTop();
    };


    const goToTop = () => {
        window.scrollTo({
            top: 0,
            behavior: "smooth",
        });
    };


    const totalPages = Math.ceil(total / itemValue);

    return (
        <div className={styles.pagination}>
            <button onClick={handleClickBack} disabled={pageValue <= 1}>back</button>
            <p>
                Page {pageValue} / {totalPages}
            </p>
            <button onClick={handleClickNext} disabled={pageValue === totalPages}>next</button>
        </div>
    );
};

export default Pagination;
