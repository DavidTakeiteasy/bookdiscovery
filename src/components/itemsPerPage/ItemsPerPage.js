import React from "react";
import styles from "./itemsPerPage.module.scss";

const ItemsPerPage = ({itemValue, setItemValue, configuration, fetchData, searchValue, setPageValue}) => {
    const handleChange = (event) => {
        searchValue && fetchData(event.target.value, 1);
        setItemValue(event.target.value);
        setPageValue(1);
    };

    return (
        <div className={styles.itemsPerPage}>
            <select value={itemValue} onChange={handleChange}>
                {configuration.map((value, index) => (
                    <option key={index} value={value}>
                        {`${value} books`}
                    </option>
                ))}
            </select>
        </div>
    );
};

export default ItemsPerPage;
