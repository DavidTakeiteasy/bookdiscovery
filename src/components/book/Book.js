import React, { useState } from "react";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import Modal from "@mui/material/Modal";
import styles from "./book.module.scss";
import noImageSmall from "../../images/noImageSmall.svg";
import noImageMedium from "../../images/noImageMedium.svg";

const Book = ({
    cover_i,
    coverSizeSmall,
    coverSizeMedium,
    coverSizeLarge,
    title,
    first_publish_year,
    isbn,
    author_name,
    viewOption,
}) => {
    const [isOpen, setOpen] = useState(false);
    const imageUrl = "https://covers.openlibrary.org/b/id/";
    const imageSmall = `${imageUrl}${cover_i}-${coverSizeSmall}.jpg`;
    const imageMedium = `${imageUrl}${cover_i}-${coverSizeMedium}.jpg`;
    const imageLarge = `${imageUrl}${cover_i}-${coverSizeLarge}.jpg`;
    const manyAuthors = author_name?.length > 3;

    const handleToggle = () => {
        setOpen(!isOpen);
    };

    const style = {
        position: "absolute",
        top: "50%",
        left: "50%",
        transform: "translate(-50%, -50%)",
        bgcolor: "background.paper",
        boxShadow: 24,
        p: 1,
    };

    const [openModal, setOpenModal] = React.useState(false);
    const handleOpen = () => setOpenModal(true);
    const handleClose = () => setOpenModal(false);

    const BookGrid = () => (
        <>
            <div className={styles.bookGrid}>
                <div className={styles.image}>
                    <img
                        src={cover_i ? imageSmall : noImageSmall}
                        loading="lazy"
                        alt="title"
                        onClick={cover_i && handleOpen}
                    />
                </div>
                <div className={isOpen ? undefined : `${styles.closed}`}>
                    <h5>{title}</h5>
                    <p>
                        {author_name?.map((name, index) => (
                            <span key={index}>{name}</span>
                        ))}
                    </p>
                    {manyAuthors && (
                        <button onClick={handleToggle}>
                            {isOpen ? "Close" : "Show all"}
                        </button>
                    )}
                </div>
            </div>
        </>
    );

    const BookList = () => (
        <>
            <div className={styles.bookList}>
                <div className={styles.image}>
                    <img
                        src={cover_i ? imageMedium : noImageMedium}
                        loading="lazy"
                        alt="title"
                        onClick={cover_i && handleOpen}
                        key={cover_i}
                    />
                </div>
                <div className={styles.content}>
                    <h4>{title}</h4>
                    <span>First published:</span>
                    <p>{first_publish_year}</p>

                    <span>ISBN:</span>
                    <p>{isbn[0]}</p>

                    <span>
                        {author_name?.length > 1 ? "Authors" : "Author"}
                    </span>
                    <p>{author_name?.join(", ")}</p>
                    {isbn[0] && (
                        <a
                            href={`https://www.amazon.de/s?k=${isbn[0]}`}
                            target="_blank"
                            rel="noreferrer"
                        >
                            Buy at amazon
                        </a>
                    )}
                </div>
            </div>
        </>
    );

    const ImageModal = () => (
        <Modal
            open={openModal}
            onClose={handleClose}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
            key={imageLarge}
        >
            <Box sx={style}>
                <img
                    src={imageLarge}
                    loading="lazy"
                    alt="title"
                    onClick={handleOpen}
                    key={cover_i}
                />
                <Typography id="modal-modal-title">{title}</Typography>
            </Box>
        </Modal>
    );

    return viewOption === "grid" ? (
        <>
            <BookGrid />
            <ImageModal />
        </>
    ) : (
        <>
            <BookList />
            <ImageModal />
        </>
    );
};

Book.defaultProps = {
    cover_i: null,
    coverSizeSmall: "S",
    coverSizeMedium: "M",
    coverSizeLarge: "L",
    isbn: ["unknown"],
    title: "unknown",
    author_name: ["unknown"],
    first_publish_year: "unknown",
};

export default Book;
