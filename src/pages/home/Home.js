import React from "react";
import styles from "./home.module.scss";
import configuration from "../../data/configuration.json";
import SearchField from "../../components/searchField/SearchField";
import ItemsPerPage from "../../components/itemsPerPage/ItemsPerPage";
import ViewOption from "../../components/viewOption/ViewOption";
import Pagination from "../../components/pagination/Pagination";
import logo from "../../images/iconLogo.png";

import Book from "../../components/book/Book";

const Home = () => {
    const [data, setData] = React.useState(null);
    const [error, setError] = React.useState(null);
    const [searchValue, setSearchValue] = React.useState("");
    const [viewOption, setViewOption] = React.useState(
        configuration.viewOption[0]
    ); // first view-option is default
    const [itemValue, setItemValue] = React.useState(
        configuration.itemsPerPage[0]
    ); // first item-per-page value is default
    const [pageValue, setPageValue] = React.useState(configuration.defaultPage); // page 1 is the default page
    const totalPages = data && Math.ceil(data.numFound / itemValue);

    const fetchData = async (itemValue, pageValue) => {
        return fetch(
            `https://openlibrary.org/search.json?title=${searchValue}&limit=${itemValue}&page=${pageValue}`
        )
            .then((response) => response.json())
            .then((data) => setData(data));
    };

    return (
        <div className={styles.home}>
            <div className={styles.navigation}>
                <div className={styles.logo}>
                    <img src={logo} alt="" />
                    Book <span>Discovery</span>
                </div>
                <SearchField
                    searchValue={searchValue}
                    setSearchValue={setSearchValue}
                    fetchData={fetchData}
                    itemValue={itemValue}
                    setPageValue={setPageValue}
                />
                <div className={styles.options}>
                    <ViewOption
                        viewOption={viewOption}
                        setViewOption={setViewOption}
                        configuration={configuration.viewOption}
                    />
                    <ItemsPerPage
                        itemValue={itemValue}
                        setItemValue={setItemValue}
                        configuration={configuration.itemsPerPage}
                        fetchData={fetchData}
                        searchValue={searchValue}
                        setPageValue={setPageValue}
                    />
                </div>
                <div className={styles.navigationBackground}></div>
            </div>

            <div className={styles.booksWrapper}>
                {error && <div>Error...</div>}

                {!data && (
                    <div className={styles.hello}>
                        <h1>Hello there!</h1>
                        <h3>
                            You can search for a book title to discover new
                            books!
                        </h3>
                    </div>
                )}

                {data && data.numFound === 0 && (
                    <h3 className={styles.noResults}>
                        There are no results <br />
                        😢
                    </h3>
                )}

                {data && data.numFound > 0 && (
                    <h4 className={styles.results}>
                        There are {data.numFound} books for you, {itemValue} on
                        each page, showing page {pageValue} of {totalPages}.
                    </h4>
                )}

                {data && (
                    <div
                        className={
                            viewOption === "grid"
                                ? `${styles.grid}`
                                : `${styles.list}`
                        }
                    >
                        {data.docs.map((book, index) => (
                            <Book
                                {...book}
                                key={index}
                                viewOption={viewOption}
                            />
                        ))}
                        <Pagination
                            pageValue={pageValue}
                            setPageValue={setPageValue}
                            itemValue={itemValue}
                            total={data.numFound}
                            fetchData={fetchData}
                        />
                    </div>
                )}
            </div>
        </div>
    );
};

export default Home;
